﻿using GoogleMeasurementProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parmik.CS.Core.Analytics
{
    public class Analytics
    {
        public static void SetID(string id)
        {
            factory = new GoogleAnalyticsRequestFactory(id);
        }
        static GoogleAnalyticsRequestFactory factory = null;
        public static void Hit(string userid, string ip, string eventCategory, string eventName)
        {
            try
            {
                var request = factory.CreateRequest(HitTypes.Event);
                request.Parameters.Add(new GoogleMeasurementProtocol.Parameters.User.UserId(userid));
                request.Parameters.Add(new GoogleMeasurementProtocol.Parameters.EventTracking.EventAction(eventName));
                request.Parameters.Add(new GoogleMeasurementProtocol.Parameters.EventTracking.EventCategory(eventCategory));
                request.Parameters.Add(new GoogleMeasurementProtocol.Parameters.Session.IpOverride(ip));
                var clientId = userid;
                request.Post(clientId);
            }
            catch (Exception ex)
            {
                //throw;
            }
        }
    }
}
