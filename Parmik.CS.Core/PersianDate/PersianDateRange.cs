﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parmik.CS.Core.PersianDate
{
	public struct PersianDateRange
	{
		public PersianDateRange(PersianDateTime start, PersianDateTime end)
			: this()
		{
			Start = start;
			End = end;
            if (start.ToGorgian() >= end.ToGorgian())
                throw new NotSupportedException("Start date shoulb be less than end date.");
        }

		public PersianDateTime Start { get; set; }
		public PersianDateTime End { get; set; }

        public TimeSpan TimeSpan { get { return End.ToGorgian().Subtract(Start.ToGorgian()); } }

        public static PersianDateRange GetFromStartOfYear(PersianDateTime end = null)
        {
            PersianDateTime endDate = end ?? PersianDateTime.Now;
            PersianDateTime startDate = endDate.ToGorgian().ToPersianDateTime();
            startDate.Month = 1;
            startDate.Day = 1;
            return new PersianDateRange(startDate, endDate);
        }

		public static PersianDateRange GetYear(PersianDateTime end = null)
		{
			PersianDateTime endDate = end ?? PersianDateTime.Now;
            PersianDateTime startDate = endDate.ToGorgian().ToPersianDateTime();
			startDate.Month = 1;
			return new PersianDateRange(startDate, endDate);
		}

		public static PersianDateRange GetMonth(PersianDateTime end = null)
		{
			PersianDateTime endDate = end ?? PersianDateTime.Now;
            PersianDateTime startDate = endDate.ToGorgian().ToPersianDateTime();
			startDate.Day = 1;
			return new PersianDateRange(startDate, endDate);
		}

	}
}
